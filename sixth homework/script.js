// ## Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// 2. Які засоби оголошення функцій ви знаєте?
// 3. Що таке hoisting, як він працює для змінних та функцій?

// ## Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.



let userName = prompt('Enter your name');
let userLastName = prompt('Enter your last name');
let userBirthDay = prompt('Enter your birthday. dd.mm.yyyy');

function createNewUser(name, surname, birthDay) {
    const newUser = {
        firstName: name,
        lastName: surname,
        userBirthDay: birthDay,
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getAge: function() {
            let date = new Date();
            let year = this.userBirthDay.slice(6);
            let month = this.userBirthDay.slice(3, 5);
            let day = this.userBirthDay.slice(0, 2);
            let age = date.getFullYear() - year;
            if (month > date.getMonth() || (month === date.getMonth() && day > date.getDay())) {
                age--
            }

            return age;
        },
        getPassword: function() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.userBirthDay.slice(6).toLowerCase()
        },


    };

    return newUser;
}

let newUser = createNewUser(userName, userLastName, userBirthDay);
let newUserAge = newUser.getAge();
console.log(newUserAge);
let newUserPassword = newUser.getPassword();
console.log(newUserPassword);











// let date = new Date();
//             let year = this.userBirthDay.slice(6);
//             console.log(year);
//             let month = this.userBirthDay.slice(3, 5);
//             let day = this.userBirthDay.slice(0, 2);
//             let age = date.getFullYear() - year;
//             console.log(age);
//             console.log(month);
//             console.log(day);
//             console.log(date.getMonth());
//             if (month > date.getMonth() || (month === date.getMonth() && day > date.getDay())) {
//                 age--;
//             }