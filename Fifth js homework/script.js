// ##
// Теоретичні питання
// 1. Опишіть своїми словами, що таке метод об 'єкту
// Це коли в об'єкті є функція. Тобто в об'єкті є якісь властивості, а за ці властивості відповідає функція.

// 2. Який тип даних може мати значення властивості об 'єкта?
// Може бути будь який тип данних

// 3. Об 'єкт це посилальний тип даних. Що означає це поняття?
// Коли значення типу данних залежить від іншого данних

// ##
// Завдання

// Реалізувати функцію створення об 'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// ####
// Технічні вимоги:

//     -Написати функцію `createNewUser()`, яка буде створювати та повертати об 'єкт `newUser`. -
//     При виклику функція повинна запитати ім 'я та прізвище. -
//     Використовуючи дані, введені юзером, створити об 'єкт `newUser` з властивостями `firstName` та `lastName`. -
//     Додати в об 'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'
//  єднану з прізвищем, все в нижньому регістрі(наприклад, `Ivan Kravchenko → ikravchenko`). -
//     Створити юзера за допомогою функції createNewUser().Викликати у цього юзера функцію `getLogin()`.Вивести у консоль результат виконання функції.

function createNewUser() {
    let userName = prompt('Enter your name');
    let userLastName = prompt('Enter your last name');
    const newUser = {
        firstName: '',
        lastName: '',
        getLogin: function() {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        }
    };
    newUser.firstName = userName;
    newUser.lastName = userLastName;

    return newUser;
}

let newUser = createNewUser();
newUser = newUser.getLogin();
console.log(newUser)